import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

// 获取用户的基本信息(名称)
export function getProfile(token) {
  return request({
    url: '/sys/profile',
    method: 'POST'
  })
}

// 获取用户的详细信息(头像)
export function getUserInfoById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

export function logout() {

}
