import request from '@/utils/request'

export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

/**
 * @description: 获取部门负责人
 * @param {*}
 * @return {*}
 */
export function getEmployeesSimple() {
  return request({
    url: '/sys/user/simple'
  })
}

// 新增子部门
export function addDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

/**
 * @description: 获取部门详情
 * @param {*} id 表示当前要编辑项的id值
 * @return {*}
 */
export function getDepartmentsDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
}

/**
  * @description: 编辑部门
  * @param {code,introduce,manager,name,pid} data
  * @return
  */
export function editDepartments(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}

/**
 * @description: 删除部门
 * @param {*} id 当前点击的操作按钮 所在的项的id
 * @return {*}
 */
export function delDepartment(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}
