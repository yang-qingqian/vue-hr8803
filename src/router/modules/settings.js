import Layout from '@/layout'
export default {
  // hidden:true,控制当前路由是否在左侧菜单显示
  path: '/settings',
  component: Layout,
  children: [
    {
      // 组织架构
      // 如果子级路由mpath设置为空'',name会自动匹配
      path: '',
      name: 'settings',
      component: () => import('@/views/settings/settings.vue'),
      // 还可以传入element-ul里面的图标,el-icon开头
      // 设置在左侧菜单显示标题和图标
      meta: { title: '公司设置', icon: 'tree' }
    }
  ]
}
