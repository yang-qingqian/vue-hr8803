import Layout from '@/layout'
export default {
  // hidden:true,控制当前路由时候是否在左侧菜单显示
  path: '/employees',
  component: Layout,
  children: [
    {
    // 员工管理
      path: '',
      name: 'employees',
      component: () => import('@/views/employees/employees.vue'),
      // 还可以传入element-ui里面的标题,el-icon开头
      // 设置在左侧菜单显示标题和图标
      meta: { title: '员工管理', icon: 'people' }
    }
  ]
}
