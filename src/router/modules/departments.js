import Layout from '@/layout'
export default {
  // hidden:true,控制当前路由时候是否在左侧菜单显示
  path: '/departments',
  component: Layout,
  children: [
    {
    // 组织架构
    // 如果子级路由,path设置为空'',name会自动匹配
      path: '',
      name: 'departments',
      component: () => import('@/views/departments/departments.vue'),
      // 还可以传入element-ui里面的标题,el-icon开头
      // 设置在左侧菜单显示标题和图标
      meta: { title: '组织架构', icon: 'tree' }
    }
  ]
}
