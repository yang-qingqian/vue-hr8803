import router from '@/router'
import store from '@/store'
import NProgress from 'nprogress' // 引入NProgress
import 'nprogress/nprogress.css' // 引入进度条的样式

import getPageTitle from '@/utils/get-page-title'

// NProgress.start()  // 开启进度条---> 何时开启
// NProgress.done()  // 关闭进度条---> 何时关闭

// to: 要去哪个页面
// from : 从哪里来
// next : 她是一个函数
// 如果直接放行  next()
// 如果要调到其他页  next(其他页)

const whiteList = ['/login', '/404']
router.beforeEach(async(to, from, next) => {
  // 动态设置网页的标题
  // document.title = '人力资源管理系统' + to.meta.title
  document.title = getPageTitle(to.meta.title)
  console.log(to.path, '去哪里')
  console.log(from.path, '从哪里来')
  NProgress.start()

  const token = store.state.user.token
  if (token) {
    // 已经登录了
    if (to.path === '/login') {
      console.log('已经登录了,不能再去登录页,跳转到首页')
      next('/')
      NProgress.done()
    } else {
      console.log('已经登录不是去登录页,放行')
      // 判断vuex中有没有用户的数据,如果没有那么久发送你个请求,有直接跳转页面
      if (!store.state.user.userInfo.userId) {
        // 1.调用actions中哈数,获取用户信息存储到vuex中
        await store.dispatch('user/setUserInfo')
      }
      next() // 放行到首页
    }
  } else {
    // 没有登录
    // 是否白名单,不需要token,/login ||404
    if (whiteList.includes(to.path)) {
      console.log('拥有白名单,直接放行')
      next()
    } else {
      console.log('未登录 没有白名单,跳转登录页')
      next('/login')
      NProgress.done()
    }
  }
})

// 页面跳转完成会经过这里
router.afterEach(() => {
  NProgress.done()
})
