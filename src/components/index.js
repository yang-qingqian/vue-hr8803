import PageTools from '@/components/PageTools'

// 自定义插件
export default {
  // 形参obj接收是Vue构造函数
  install(Vue) {
    // 插件中全局注册组件
    Vue.component('page-tools', PageTools)
  }
}
