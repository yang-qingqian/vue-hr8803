import { login, getProfile, getUserInfoById } from '@/api/user'
import { getToken, removeToken, setTimeStamp, setToken } from '@/utils/auth'
export default {
  namespaced: true,
  state: {
    token: getToken() || '',
    userInfo: {}
  },
  // 唯一修改数据的地方
  mutations: {
    setToken(state, token) {
      state.token = token
      setToken(token)
    },
    delToken(state) {
      state.token = ''
      removeToken()
    },
    setUserInfo(state, info) {
      state.userInfo = info
    },
    delUserInfo(state) {
      state.userInfo = {}
    }
  },
  actions: {
    // 退出登录情况token,用户信息
    logout(context) {
      context.commit('delToken')
      context.commit('delUserInfo')
    },
    // 2.获取用户信息存储到vue中
    async setUserInfo(context) {
      try {
        const res = await getProfile()
        console.log(res, 'setUserInfo')
        const info = await getUserInfoById(res.data.userId)
        console.log(info, 'getUserInfoById')
        // 合并用户的名称和用户的头像到一个js对象中
        context.commit('setUserInfo', { ...res.data, ...info.data })
      } catch (error) {
        console.log(error, '获取用户信息失败')
      }
    },
    async userLogin(context, formData) {
      console.log(context, 'context')
      // context 是store的实例,context.commit
      // 发送实现登录,获取token,存储vuex中
      const res = await login(formData)
      console.log(res, 'res')
      context.commit('setToken', res.data)
      // 调用存储获取token时的时间戳
      setTimeStamp()
    }
  }
}

