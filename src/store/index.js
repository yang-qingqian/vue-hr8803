import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const req = require.context('./modules', false, /\.js$/)
console.log(req, 'req')
console.log(req.keys(), 'req.keys()')

const mName = req.keys().map(item => item.split('./')[1]).map(item => item.split('.js')[0])
console.log(mName, 'mName')
console.log(req.keys().map(req), 'map')

const mModule = req.keys().map(req)

const obj = {}
mName.forEach((item, index) => {
  console.log(item, index)
  obj[item] = mModule[index].default
})
console.log(obj, 'obj')

// 总仓库
const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user
  },
  getters
})

export default store
