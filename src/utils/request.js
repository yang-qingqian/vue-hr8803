import store from '@/store'
import axios from 'axios'
import router from '@/router'
import { getTimeStamp } from '@/utils/auth'
// 设置token的过期时间
const expireTime = 360000

// 设置一个检查是否过期的函数
function isCheckTimeOut() {
  // (当前的时间戳 - 存储token的时间戳) /1000
  // 获取存储token的时间戳
  const tokenTime = getTimeStamp()
  return (Date.now() - tokenTime) / 1000 > expireTime
}

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API
})

// 请求数据
// service.interceptors.request.use()
// 添加请求拦截器
service.interceptors.request.use(function(config) {
  console.log('所有正常的请求在到大服务器之前一定会经过这里')
  console.log(config, 'config')
  if (store.state.user.token) {
    // 注入token
    // 删除token,清空用户信息
    if (isCheckTimeOut()) {
      // token过期了
      // 删除token,清空用户信息
      store.dispatch('user/logout')
      router.push('/login')
      // 返回错误的一个响应\
      return Promise.reject(new Error('token超时!'))
    }
    config.headers['Authorization'] = `Bearer ${store.state.user.token}`
  }
  // 终点注意: 一定要return 给服务器
  return config
}, function(error) {
  // 对请求错误做些什么
  console.log('所有错误的请求在到达服务器之前一定会经过这里')
  return Promise.reject(error)
})
// 响应式数据
service.interceptors.response.use(function(response) {
  if (!response.data.success) return Promise.reject(new Error('登录失败!'))
  return response.data
}, function(error) {
  // 对响应错误做点什么
  // 判断响应是否是token超时
  if (error.response.status === 401 && error.response.data.code === 10002) {
    // 清空token,清空用户信息
    store.dispatch('user/logout')
    // 跳转到登录页面
    router.push('/login')
    return Promise.reject(error)
  }
})
export default service
