import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'

const timeKey = 'vue_admin_template_token' // 存储token
// 从cookies中获取token存储的时间戳
export function getTimeStamp() {
  return Cookies.get(timeKey)
}

// 存储获取到token时的时间戳
export function setTimeStamp() {
  return Cookies.set(timeKey, Date.now)
}

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
